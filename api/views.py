from django.shortcuts import render

# Create your views here.

from rest_framework import viewsets
from .serializers import *
from .models import *
from rest_framework.views import APIView
from rest_framework.response import Response
from .Helper import *
import random

# Login


class Login(APIView):
    def post(self, request):
        try:
            user = Users.objects.filter(
                e_mail=request.data['email'], password=request.data['password'])
            if user.exists():
                serializer = LoginSerializer(user[0])
                return Response(serializer.data)
            else:
                return Response({'message': 'Please check the email and password !!'}, status=401)
        except Exception as e:
            return Response({'message': 'Missing {}'.format(str(e))}, status=401)


class ForgetPasword(APIView):
    def get(self, request, email):
        try:
            user = Users.objects.filter(e_mail=email)
            if user.exists():
                otp = random.randint(1000, 9999)
                user.update(otp=otp)
                sendMail('Reset password', 'Your OTP to reset passowrd is {}'.format(
                    str(otp)), to=[email])
                return Response({'message': 'Otp sent to your mail id !!'})
            else:
                return Response({'message': 'No user exists with this mail id'}, status=300)
        except Exception as e:
            return Response({'message': 'Missing {}'.format(str(e))}, status=401)

    def put(self, request):
        try:
            user = Users.objects.get(e_mail=request.data['email'])
            if user.otp == request.data['otp']:
                return Response({'message': 'Otp Verified Successfully Please Enter New Password.', 'status': True})
            else:
                return Response({'message': 'Invalid Otp', 'status': False})
        except Exception as e:
            return Response({'message': 'Missing {}'.format(str(e))}, status=404)

    def post(self, request):
        try:
            Users.objects.filter(e_mail=request.data['email']).update(
                password=request.data['password'])
            return Response({'message': 'Password Updated Successfully', 'status': True})
        except Exception as e:
            return Response({'message': 'Missing {}'.format(str(e))}, status=500)


class SitesProjectsClass(APIView):
    def get(self,request):
        try:
            sites = ProjectSites.objects.filter(active='1').values('id','city__name','site_name','image_url','description','location','start_date','end_date')
            return Response(sites)
        except Exception as e:
            return Response({'message': 'Missing {}'.format(str(e))}, status=401)


class GetMessage(APIView):

    # def get(self,request):
    #     # name = Users.objects.all().values()
    #     projectList = Projects.objects.filter(pr_name='sakib').values()
    #     return Response(projectList)

    # def post(self,request):
    #     try:
    #         userObj = Users.objects.create(name=request.data['name'],e_mail=request.data['email'],mobile_no=request.data['mobile'])
    #         serializer=UsersSerializers(userObj)
    #         return Response(serializer.data)
    #     except Exception as e:
    #         return Response({"message":"Missing {}".format(str(e))},status=401)

    def put(self, request):
        try:
            users = Users.objects.filter(id=request.data['id'])
            if not users.exists():
                user = Users.objects.filter(mobile_no=request.data['mobile'])
                if user.exists():
                    return Response({"Message": "User Id is Already present", 'user_id': user[0].id})
                objUser = Users.objects.create(
                    name=request.data['name'], e_mail=request.data['email'], mobile_no=request.data['mobile'])
                request.data['id'] = objUser.id
                # return Response({'user_id':objUser.id,'bn_id':request.data['banner_id']})

            userVisitObj = UserVisitedProjects.objects.create(
                br_pr_id=request.data['project_id'], user_pr_id=request.data['id'], visite_date=request.data['visit_date'], ip_address=request.data['ip_address'])
            projectsObj = Projects.objects.get(
                pr_id=request.data['project_id'])
            serializers = ProjectSerializer(projectsObj)
            dict = serializers.data
            dict['user_id'] = request.data['id']
            # userVisitObj = UserVisitedProjects.objects.create(user_pr_id=userObj[0]['id'],br_pr_id=request.data['id'],visite_date = request.data['visite_date'],ip_address=request.data['ip'])
            # serializer = UserVisitedProjectSerializers(userVisitObj)
            return Response(dict)
        except Exception as e:
            return Response({"message": "Missing {}".format(str(e))}, status=401)

    # def put(self,request):
    #     try:
    #         pass
    #     except Exception as e:
    #         return Response({"Message:Missing {}".format(str(e))}.status=401
    #                         )
