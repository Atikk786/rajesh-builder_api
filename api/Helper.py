from django.core.mail import send_mail,EmailMultiAlternatives

def sendMail(subject,message,to=[]):
    from_email = 'rajeshbuilder@gmail.com'
    msg = EmailMultiAlternatives(subject,message,from_email, to)
    msg.send()
