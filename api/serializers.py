from rest_framework import serializers
from api.models import *

class  LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = "__all__"
        
class ProjectSitesSerializer(serializers.ModelSerializer):
    class Meta:
        model = SiteProjects
        fields = "__all__"