from django.contrib import admin
from django.urls import include, path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *

urlpatterns = [
   # path('send_msg/<int:id>/',GetMessage.as_view(),name='sendmsg'),
   path('send_msg/',GetMessage.as_view(),name='sendmsg1'),
   path('login/<str:email>/',Login.as_view(),name='login'),
   path('login/',Login.as_view(),name='login'),
   path('forgetpassword/',ForgetPasword.as_view(),name='forget_password'),
   path('forgetpassword/<str:email>/',ForgetPasword.as_view(),name='forget_password'),
   path('projects/',SitesProjectsClass.as_view(),name='projectSites')
   
]